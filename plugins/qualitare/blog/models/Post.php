<?php

namespace Qualitare\Blog\Models;

use Model;
use BackendAuth;

/**
 * Post Model
 */
class Post extends Model {

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
	public $translatable = ['title','excerpt', 'content'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_blog_posts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array With fields
     */
    protected $with = ['thumbnail'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'category' => 'Qualitare\Blog\Models\Category',
        'especiality' => 'Qualitare\Hnsn\Models\Especialidade'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'thumbnail' => 'System\Models\File'
    ];
    public $attachMany = [];
}
