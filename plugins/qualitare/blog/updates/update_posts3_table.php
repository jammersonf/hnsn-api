<?php namespace Qualitare\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePosts3Table extends Migration
{
	public function up()
	{
		Schema::table('qualitare_blog_posts', function(Blueprint $table) {
			$table->boolean('structure')->default(0);
		});
	}

	public function down()
	{
		Schema::table('qualitare_blog_posts', function(Blueprint $table) {
			$table->dropColumn('structure');
		});
	}
}
