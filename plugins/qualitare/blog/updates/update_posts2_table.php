<?php namespace Qualitare\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdatePostsTable extends Migration
{
	public function up()
	{
		Schema::table('qualitare_blog_posts', function(Blueprint $table) {
			$table->dropColumn('especiality_id');
		});

		Schema::table('qualitare_blog_posts', function(Blueprint $table) {
			$table->integer('especiality_id')->nullable();
		});
	}

	public function down()
	{
	}
}
