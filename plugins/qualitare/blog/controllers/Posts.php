<?php namespace Qualitare\Blog\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Qualitare\Blog\Models\Post;
use Qualitare\Blog\Models\Category;
use Illuminate\Http\Request;

/**
 * Posts Back-end Controller
 */
class Posts extends Controller
{
	public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController'
	];

	public $formConfig = 'config_form.yaml';
	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('Qualitare.Blog', 'menu-blog');
	}

	public function show(Request $request, $slug)
	{
		$related = [];
		$post = Post::where('slug', $slug)
			->first();

		$post->translateContext($request->header('Content-Language'));

		// No post was found
		if (!$post)
			return response(['errors' => ['Resource not found']], 404);

		//Increments number of views	
		$post->views = $post->views + 1;
		$post->save();


		//Related
		/*if (!empty($post)) {
			$related = Post::where('category_id', $post->category_id)
				->where('id', '!=', $post->id)
				->limit(3)
				->get();

			foreach($related as $post):
				$post->translateContext($request->header('Content-Language'));
			endforeach;
		}*/

		return response(['post' => $post, /*'related' => $related*/], 200);
	}

	public function all(Request $request){
	    $posts = Post::where('featured', '0');
	    if(!empty($limit))
	        $posts = $posts->limit($limit);

	    if ($request->has('especiality'))
	    	$posts = $posts->where('especiality_id', $request->especiality);	    

	    if ($request->has('structure'))
	    	$posts = $posts->where('structure', $request->structure);

        $posts = $posts->orderBy('created_at', 'desc')->get();

        foreach($posts as $post):
            $post->translateContext($request->header('Content-Language'));
        endforeach;

        return response($posts,200);
    }

	public function list(Request $request, $slug)
	{
		$featured = Post::where('featured', '1')->orderBy('created_at', 'desc')->limit(3)->get();
		$posts = Post::whereNotIn('id', $featured->lists('id'))->whereHas('category', function($query) use ($slug) {
			return $query->where('slug', $slug);
		})->get();

		foreach($posts as $post):
			$post->translateContext($request->header('Content-Language'));
		endforeach;

		// No post was found
		if ($posts->total() == 0)
			return response(['errors' => ['Resource not found']], 404);

		return response($posts, 200);
	}

	public function featured(Request $request)
	{
		$posts = Post::where('featured', '1')->orderBy('created_at', 'desc')->get();
		foreach($posts as $post):
			$post->translateContext($request->header('Content-Language'));
		endforeach;

		// No featured post was found
		if (count($posts) == 0)
			return response(['errors' => ['Resource not found']], 404);
		return response($posts, 200);
	}

	public function related(Request $request, $id)
	{
		$post = Post::find($id);
		
		if(!empty($post)){
			$related = Post::where('category_id', $post->category_id)->where('id', '!=', $post->id)->get();
			foreach($related as $post):
				$post->translateContext($request->header('Content-Language'));
			endforeach;
			return response($related, 200);
		}else{
			return response([], 200);
		}
	}

	public function search(Request $request, $keyword)
	{
		$posts = Post::whereHas('category', function($query) use ($keyword) {
			return $query->where('name', 'LIKE', "%{$keyword}%")
				->orWhere('slug', 'LIKE', "%{$keyword}%");
		})
			->orWhere('title', 'LIKE', "%{$keyword}%")
			->orWhere('slug', 'LIKE', "%{$keyword}%")
			->orWhere('content', 'LIKE', "%{$keyword}%")
			->paginate();

		foreach($posts as $post):
			$post->translateContext($request->header('Content-Language'));
		endforeach;
		// No post was found
		if ($posts->total() == 0)
			return response(['errors' => ['Resource not found']], 404);

		return response($posts, 200);
	}
}
