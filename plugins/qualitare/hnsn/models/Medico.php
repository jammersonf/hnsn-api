<?php namespace Qualitare\Hnsn\Models;

use Model;

/**
 * Model
 */
class Medico extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    protected $with = ['foto', 'especialidade'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_hnsn_medicos';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array Validation rules
     */
    public $fillable = [
			'name', 'especialidade', 'foto', 'crm', 'description'
    ];

    public $attachOne = [
        'foto' => 'System\Models\File'
    ];

    public $belongsToMany = [
            'especialidade' => [
            'Qualitare\Hnsn\Models\Especialidade',
            'table' => 'qualitare_hnsn_especialidade_medico'
        ]
    ];
}
