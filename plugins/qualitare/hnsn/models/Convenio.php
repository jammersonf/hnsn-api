<?php namespace Qualitare\Hnsn\Models;

use Model;

/**
 * Model
 */
class Convenio extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    protected $with = ['foto'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_hnsn_convenios';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
      'foto' => 'System\Models\File'
    ];

		public $fillable = [
			'name', 'tipo', 'desc', 'foto'
		];

    public function getTipoOptions(){
        return [
                 'maternidade' => 'Maternidade',
                 'adultos' => 'Adultos',
                 'pediatria' => 'Pediatria'
               ];
    }

    public function getTipoAttribute($tipo){
        return ucfirst($tipo);
    }

}
