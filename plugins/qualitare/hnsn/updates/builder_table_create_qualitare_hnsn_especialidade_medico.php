<?php namespace Qualitare\Hnsn\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareHnsnEspecialidadeMedico extends Migration
{
    public function up()
    {
        Schema::create('qualitare_hnsn_especialidade_medico', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('especialidade_id');
            $table->integer('medico_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_hnsn_especialidade_medico');
    }
}
