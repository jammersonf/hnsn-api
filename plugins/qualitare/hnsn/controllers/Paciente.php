<?php


namespace Qualitare\Hnsn\Controllers;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use Mail;

class Paciente extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function sendMail(Request $request)
    {
        // Validate
        $validate = validator($request->all(),[
            'name' => 'required|max:255',
            'phone' => 'required',
            'country' => 'required|max:255',
            'message' => 'required',
        ]);


        // If fails validate
        if($validate->fails()):
            // Redirect same page with errors messages
            return response()->json(['errors' => $validate->getMessageBag()], 400);
        endif;

        // Prepare body
        $body = [
            'name'                   => $request->get('name'),
            'phone'                  => $request->get('phone'),
            'comments'               => $request->get('message'),
            'country'                => $request->get('country')
        ];

        // Send email
        Mail::send('qualitare.hnsn::foreign-patient', $body, function($message) {
            $message->to('josilene.almeida@hnsn.com.br');
            $message->subject('HNSN – Novo Paciente Internacional');
        });

        // Response
        return response(['success' => true, 'message' => 'Contact sent successfully! Please wait for our contact. :)', 200]);
    }

}
