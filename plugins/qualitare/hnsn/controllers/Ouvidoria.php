<?php


namespace Qualitare\Hnsn\Controllers;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class Ouvidoria extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function sendMail(Request $request)
    {
        // Validate
        $validate = validator($request->all(),[
            'name' => 'required|max:255',
            'type' => 'required',
            'phone' => 'required',
            'email' => 'required|email|max:255',
            'message' => 'nullable',
            'file' => 'nullable|file|mimes:pdf,doc,docx',
        ]);


        // If fails validate
        if($validate->fails()):
            // Redirect same page with errors messages
            return response()->json(['errors' => $validate->getMessageBag()], 400);
        endif;

        // Default url
        $url = 'Vazio';

        // If filled file
        if ($request->get('file')) {

            // Prepare file
            $file = Storage::disk('media')->putFile('ouvidoria', new File($request->file('file')));

            // Storage disk
            $path = '/storage/app/uploads/' . $file;

            // Prepare url
            $url = "<a href='https://api.hnsn.com.br/". $path . "' target='_blank'>Baixar</a>";

        }

        // Prepare body
        $body = [
            'name'                   => $request->get('name'),
            'type'                   => $request->get('type'),
            'email'                  => $request->get('email'),
            'phone'                  => $request->get('phone'),
            'comments'               => $request->get('message'),
            'url'                    => $url,
        ];


        // Send email
        Mail::send('qualitare.hnsn::ombudsman', $body, function($message) {
            $message->to('ouvidoria@hnsn.com.br');
            $message->subject('HNSN – Nova Ouvidoria');
        });

        // Response
        return response(['success' => true, 'message' => 'Ouvidoria enviado com sucesso! Por favor, aguarde o nosso contato :)', 200]);
    }

}
