<?php namespace Qualitare\Plano\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Http\Request;
use October\Rain\Support\Facades\Flash;
use Qualitare\Hnsn\Models\Especialidade;
use Qualitare\Plano\Models\Plan;
use Qualitare\Plano\Models\Unity;

/**
 * Unidades Back-end Controller
 */
class Unidades extends Controller
{
	public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController'
	];

	public $formConfig = 'config_form.yaml';
	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('Qualitare.Plano', 'menu-plano');
	}

	public function states()
	{
		$posts = Unity::groupBy('state')->get(['state']);

		return response($posts, 200);
	}
        
	public function list(Request $request)
	{
		$posts = new Unity();
		if ($request->state) {
			$posts = $posts->where('state', $request->state);
		}
		$posts = $posts->get();
		$unities = ['Cobertura'];
		$especialities = [];
		foreach($posts as $post) {
			$unities[$post->id] = $post->name;
		}
		$data = Especialidade::all();
		foreach($data as $especiality) {
			$arr = [];
			$arr[] = $especiality->name;
			foreach($unities as $key => $unity) {
				if ($key > 0) {
					$unityModel = Unity::find($key);
					$plan = array_keys($unityModel['plans'], $request->plan);

					$arr[] = in_array($especiality->id, $unityModel['especialities'][$plan[0]]) ? 'Sim' : 'Não';
				}
			}
			$especialities[] = $arr;
		}
		
		return response([$unities, $especialities], 200);
	}

	public function create_onSave()
	{
		$post = post();
		$plans = [];
		$especialities = [];

		foreach ($post['Unity']['plans'] as $plan) {
			$plans[] = $plan;
		}
		foreach ($post['Unity']['especialities'] as $especiality) {
			$especialities[] = $especiality;
		}
		$post['Unity']['plans'] = $plans;
		$post['Unity']['especialities'] = $especialities;

		Unity::create($post['Unity']);

		Flash::success('Unidade Salva');

		return redirect('backend/qualitare/plano/unidades');
	}

	public function formExtendFields($form)
	{
		$arr = [];
		for($i=1;$i<=Plan::count();$i++) {
	        $arr["plans[$i]"] = [
	            'label' => "Plano $i",
		        'nameFrom' => 'name',
		        'descriptionFrom' => 'slug',
		        'span' => 'left',
		        'required' => '1',
		        'type' => 'dropdown',
		        'tab' => 'Vaga',
		        'options' => 'listPlans',

	        ];
	        $arr["especialities[$i]"] = [
	            'label' => "Especialidades $i",
		        'nameFrom' => 'especilities',
		        'descriptionFrom' => 'slug',
		        'span' => 'right',
		        'required' => '1',
		        'type' => 'checkboxlist',
		        'tab' => 'Vaga',
		        'attributes' => [
            		'multiple' => true
            	],
            	'options' => 'listEspecialities'
	        
	    	];
		}
	   	
		$form->addFields($arr);
	}
}
