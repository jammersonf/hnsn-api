<?php namespace Qualitare\Plano\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Http\Request;
use Qualitare\Plano\Models\Convenio;

/**
 * Convenio Back-end Controller
 */
class Convenios extends Controller
{
	public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController'
	];

	public $formConfig = 'config_form.yaml';
	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('Qualitare.Plano', 'menu-plano');
	}
        
	public function list(Request $request)
	{
		$posts = Convenio::with(['categories', 'categories.planos'])->get();

		return response($posts, 200);
	}
}
