<?php namespace Qualitare\Plano\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Http\Request;
use Qualitare\Plano\Models\PlanCategory;

/**
 * Convenio Back-end Controller
 */
class Categorias extends Controller
{
	public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController'
	];

	public $formConfig = 'config_form.yaml';
	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('Qualitare.Plano', 'menu-plano');
	}
        
	public function list(Request $request)
	{
		$posts = PlanCategory::paginate();
		// No post was found
		if ($posts->total() == 0)
			return response(['errors' => ['Resource not found']], 404);

		return response($posts, 200);
	}
}
