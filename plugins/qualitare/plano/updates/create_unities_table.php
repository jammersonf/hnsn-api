<?php namespace Qualitare\Plano\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUnitiesTable extends Migration
{
	public function up()
	{
		Schema::create('qualitare_plano_unities', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name');
			$table->string('state');
			$table->text('plans');
			$table->text('especialities');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('qualitare_plano_unities');
	}
}
