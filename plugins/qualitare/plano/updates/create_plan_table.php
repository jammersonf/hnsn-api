<?php namespace Qualitare\Plano\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePlanTable extends Migration
{
	public function up()
	{
		Schema::create('qualitare_plano_plans', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name');
			$table->integer('category_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('qualitare_plano_plans');
	}
}
