<?php

Route::group(['prefix' => 'v1'], function () {

    //Convenios
    Route::group(['prefix' => 'convenios'], function () {
        Route::get('list', 'Qualitare\Plano\Controllers\Convenios@list');
    });

    //Categorias
    Route::group(['prefix' => 'categorias'], function () {});

    //Planos
    Route::group(['prefix' => 'planos'], function () {});

    //Unidades
    Route::group(['prefix' => 'unidades'], function () {
        Route::get('list', ['uses' => 'Qualitare\Plano\Controllers\Unidades@list']);
        Route::get('states', ['uses' => 'Qualitare\Plano\Controllers\Unidades@states']);
    });

});
