<?php namespace Qualitare\Plano\Models;

use Model;

/**
 * Category Model
 */
class Convenio extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_plano_convenios';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
	public $hasMany = [
		'categories' => ['Qualitare\Plano\Models\PlanCategory']
	];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
