<?php namespace Qualitare\Plano\Models;

use Model;

/**
 * Category Model
 */
class PlanCategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_plano_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
	public $hasMany = [
		'planos' => ['Qualitare\Plano\Models\Plan', 'key' => 'category_id']
	];
    public $belongsTo = [
        'convenio' => ['Qualitare\Plano\Models\Convenio']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
