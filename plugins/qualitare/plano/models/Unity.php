<?php namespace Qualitare\Plano\Models;

use Model;
use Qualitare\Hnsn\Models\Especialidade;
use Qualitare\Plano\Models\Plan;

/**
 * Unity Model
 */
class Unity extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_plano_unities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'state', 'plans', 'especialities'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
	public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    protected $jsonable = ['plans', 'especialities'];

    public function listPlans($fieldName, $value, $formData)
    {
        return Plan::pluck('name', 'id')->toArray();
    }

    public function listEspecialities($fieldName, $value, $formData)
    {
        return Especialidade::pluck('name', 'id')->toArray();
    }
}
